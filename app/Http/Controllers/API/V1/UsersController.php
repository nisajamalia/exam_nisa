<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException as ValidationException;
use App\Models\Users;
use Exception;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $Users = Users::query();
            $pagination = 3;
            $Users = $Users->orderBy('created_at','desc')->paginate($pagination);

            $response=$Users;
            $code=200;
        } catch (Exception $e) {
            $code=500;
            $response=$e->getMessage();
        }

        return apiResponseBuilder($code,$response);
    
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            
            'nama_siswa' => 'required',
            'nisn' => 'required',
            'ttl' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'no_tlp' => 'required',
            'jurusan' => 'required',
            'bidang_keahlian' => 'required',
            'email' => 'required',
            'image' => 'required',
            'berkas_file' => 'required'
            ]);


            try{
                $Users = new Users();
    
                $Users->nama_siswa = $request->nama_siswa;
                $Users->nisn = $request->nisn;
                $Users->ttl = $request->ttl;
                $Users->jenis_kelamin = $request->jenis_kelamin;
                $Users->agama = $request->agama;
                $Users->no_tlp = $request->no_tlp;
                $Users->jurusan = $request->jurusan;
                $Users->bidang_keahlian = $request->bidang_keahlian;
                $Users->email = $request->email;
                $Users->image = $request->image;
                $Users->berkas_file = $request->berkas_file;                
    
                $Users->save();
                $code=200;
                $response=$Users;
    
            } catch (Exception $e) {
                if($e instanceof ValidationException){
                    $code = 400;
                    $response = 'tidak ada data';
                } else {
                    $code = 500;
                    $response=$e->getMessage();
                }
            }
    
            return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $Users=Users::findOrFail($id);

            $code=200;
            $response=$Users;
        } catch (Exception $e) {
            if($e instanceof ModelNotFoundException){
                $code = 404;
                $response = 'inputkan sesuai id';
            } else {
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'nama_siswa' => 'required',
            'nisn' => 'required',
            'ttl' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'no_tlp' => 'required',
            'jurusan' => 'required',
            'bidang_keahlian' => 'required',
            'email' => 'required',
            'image' => 'required',
            'berkas_file' => 'required'
            ]);

        try{
            $Users = Users::find($id);

                $Users->nama_siswa = $request->nama_siswa;
                $Users->nisn = $request->nisn;
                $Users->ttl = $request->ttl;
                $Users->jenis_kelamin = $request->jenis_kelamin;
                $Users->agama = $request->agama;
                $Users->no_tlp = $request->no_tlp;
                $Users->jurusan = $request->jurusan;
                $Users->bidang_keahlian = $request->bidang_keahlian;
                $Users->email = $request->email;
                $Users->image = $request->image;
                $Users->berkas_file = $request->berkas_file;   
        
            $Users->save();
            $code=200;
            $response=$Users;

        } catch (Exception $e) {
            if ($e instanceof ValidationException) {
                $code = 400;
                $response = 'data tidak ada';
            } else {
                $code = 500;
                $response = $e->getMessage();
            }

        }

        return apiResponseBuilder($code, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Users=Users::find($id);
            $Users->delete();
            $code=200;
            $response=$Users;
        } catch (Exception $e) {
            $code=500;
            $response=$e->getMessage();
        }

        return apiResponseBuilder($code,$response);
    }
}
