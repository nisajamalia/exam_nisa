<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_siswa',50);
            $table->integer('nisn');
            $table->string('ttl');
            $table->string('jenis_kelamin');
            $table->string('agama');
            $table->integer('no_tlp');
            $table->string('jurusan',50);
            $table->string('bidang_keahlian');
            $table->string('email')->unique();
            $table->string('image')->nullable();
            $table->string('berkas_file')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
